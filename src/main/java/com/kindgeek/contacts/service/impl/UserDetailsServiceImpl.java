package com.kindgeek.contacts.service.impl;

//import com.kindgeek.contacts.entity.User;

import com.kindgeek.contacts.entity.UserRole;
import com.kindgeek.contacts.repository.UserRepository;
import com.kindgeek.contacts.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Yurii Ploskyi on 05.03.17.
 */
@Component
public class UserDetailsServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return loadByUser(userRepository.findByUserName(username));
    }

    @Override
    public void register(com.kindgeek.contacts.entity.User user) throws UsernameExsistException {
        if (userRepository.findByUserName(user.getUserName()) != null) {
            throw new UsernameExsistException();
        }
        userRepository.save(encodePassword(user));
    }

    private UserDetails loadByUser(com.kindgeek.contacts.entity.User user) {
        if (user == null) {
            throw new UsernameNotFoundException("user not found");
        }
        return buildUserForAuthentication(user, buildUserAuthority(user.getUserRole()));
    }

    private User buildUserForAuthentication(com.kindgeek.contacts.entity.User user, List<GrantedAuthority> authorities) {
        return new User(user.getUserName(), user.getPassword(), true, true, true, true, authorities);
    }

    private List<GrantedAuthority> buildUserAuthority(Set<UserRole> userRoles) {
        Set<GrantedAuthority> settedAuths = new HashSet<>();
        userRoles.forEach(userRole -> settedAuths.add(new SimpleGrantedAuthority(userRole.getRole())));
        return new ArrayList<>(settedAuths);
    }

    private com.kindgeek.contacts.entity.User encodePassword(com.kindgeek.contacts.entity.User user) {
        return new com.kindgeek.contacts.entity.User(user.getUserName(), passwordEncoder.encode(user.getPassword()));
    }

}

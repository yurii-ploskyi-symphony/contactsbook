package com.kindgeek.contacts.service.impl;

import com.kindgeek.contacts.entity.Contact;
import com.kindgeek.contacts.entity.User;
import com.kindgeek.contacts.repository.ContactRepository;
import com.kindgeek.contacts.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Yurii Ploskyi on 04.03.17.
 */
@Component
public class ContactServiceImpl implements ContactService {

    @Autowired
    private ContactRepository contactRepository;

    @Override
    public Contact save(Contact contact) {
        contact.setOwner(getCurrentUser());
        return contactRepository.save(contact);
    }

    @Override
    public Contact findOne(Long id) {
        return contactRepository.findOne(id);
    }

    @Override
    public List<Contact> findAll() {
        return contactRepository.findByOwnerUserName(getCurrentUserName());
    }

    @Override
    public List<Contact> findAll(Set<String> contactGroup) {
        return contactRepository.findByOwnerUserNameAndContactGroupIn(getCurrentUserName(), contactGroup);
    }

    @Override
    public List<Contact> findLike(String like) {
        String currentUserName = getCurrentUserName();
        List<Contact> contacts = contactRepository.findByFirsNameContainingOrLastNameContainingOrEmailContainingOrPhoneNumberContainingAndOwnerUserNameEquals(like, like, like, like, currentUserName);
        return contacts.stream().filter(contact -> contact.getOwner().getUserName().equals(currentUserName)).collect(Collectors.toList());
    }

    @Override
    public void delete(Contact contact) {
        try {
            contactRepository.delete(contact);
        } catch (Exception e){
            // Do nothing
        }
    }

    @Override
    public List<String> findAllGroups() {
        List<String> groups = contactRepository.findGroups(getCurrentUserName());
        return groups.stream().filter(x -> !x.equals("")).collect(Collectors.toList());
    }

    private User getCurrentUser() {
        return new User(getCurrentUserName());
    }

    private String getCurrentUserName() {
        try {
            return ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        } catch (Exception e) {
            return null;
        }
    }

}

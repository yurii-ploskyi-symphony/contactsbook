package com.kindgeek.contacts.service;

import com.kindgeek.contacts.entity.Contact;

import java.util.List;
import java.util.Set;

/**
 * Created by Yurii Ploskyi on 04.03.17.
 */
public interface ContactService {

    Contact save(Contact contact);

    Contact findOne(Long id);

    List<Contact> findAll();

    List<Contact> findAll(Set<String> contactGroup);

    List<Contact> findLike(String like);

    void delete(Contact contact);

    List<String> findAllGroups();

}

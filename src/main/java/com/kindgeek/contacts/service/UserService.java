package com.kindgeek.contacts.service;

import com.kindgeek.contacts.entity.User;
import com.kindgeek.contacts.service.impl.UsernameExsistException;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by root on 05.03.17.
 */
public interface UserService extends UserDetailsService {
    void register(User user) throws UsernameExsistException;
}

package com.kindgeek.contacts.service;

import com.kindgeek.contacts.entity.User;

/**
 * Created by Yurii Ploskyi on 05.03.17.
 */
public interface AuthenticationService {
    void authenticate(User user);
    void logout();
}

package com.kindgeek.contacts.repository;

import com.kindgeek.contacts.entity.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Yurii Ploskyi on 05.03.17.
 */
public interface UserRepository extends CrudRepository<User, String> {
    User findByUserName(String username);
}

package com.kindgeek.contacts.repository;

import com.kindgeek.contacts.entity.Contact;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Set;

/**
 * Created by Yurii Ploskyi on 04.03.17.
 */
public interface ContactRepository extends CrudRepository<Contact, Long> {

    String SELECT_GROUPS = "SELECT DISTINCT contact_group FROM contact where user_name = ?1";

//    List<Contact> findByFirsNameContainingOrLastNameContainingOrEmailContainingOrPhoneNumberContainingAndOwnerUserName(String firstName, String lastName, String email, String phoneNumber, String userName);
List<Contact> findByFirsNameContainingOrLastNameContainingOrEmailContainingOrPhoneNumberContainingAndOwnerUserNameEquals(String firstName, String lastName, String email, String phoneNumber, String userName);

    List<Contact> findByOwnerUserName(String userName);

    List<Contact> findByOwnerUserNameAndContactGroupIn(String userName, Set<String> contactGroup);

    @Query(value = SELECT_GROUPS, nativeQuery = true)
    List<String> findGroups(String username);


}

package com.kindgeek.contacts.entity;

import javax.persistence.*;

/**
 * Created by Yurii Ploskyi on 04.03.17.
 */
@Entity
public class Contact {

    @Id
    @GeneratedValue
    private Long id;

    private String firsName = "";
    private String lastName = "";
    private String email = "";
    private String phoneNumber = "";
    private String contactGroup = "";

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userName")
    private User owner;

    public Contact() {
    }

    public Contact(String firsName, String lastName, User owner) {
        this.firsName = firsName;
        this.lastName = lastName;
        this.owner = owner;
    }

    public String getFirsName() {
        return firsName;
    }

    public void setFirsName(String firsName) {
        this.firsName = firsName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdAsString() {
        return String.valueOf(id);//id.toString();
    }

    public void setIdAsString(String id) {
        try {
            this.id = Long.valueOf(id);
        } catch (NumberFormatException e) {
            //Do nothing
        }
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getContactGroup() {
        return contactGroup == null ? "" : contactGroup;
    }

    public void setContactGroup(String contactGroup) {
        this.contactGroup = contactGroup;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", firsName='" + firsName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
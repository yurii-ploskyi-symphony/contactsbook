package com.kindgeek.contacts.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by Yurii Ploskyi on 04.03.17.
 */
@SpringBootApplication
@ComponentScan("com.kindgeek.contacts")
@EntityScan("com.kindgeek.contacts.entity")
@EnableJpaRepositories(basePackages = "com.kindgeek.contacts.repository")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}

package com.kindgeek.contacts.main;

import com.kindgeek.contacts.entity.Contact;
import com.kindgeek.contacts.entity.User;
import com.kindgeek.contacts.repository.ContactRepository;
import com.kindgeek.contacts.repository.UserRepository;
import com.kindgeek.contacts.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;

/**
 * Created by Yurii Ploskyi on 04.03.17.
 */
@RestController
@Component
public class MainController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    void init() {
        importData();
    }

    public void importData() {
        User user = new User();
        user.setUserName("1");
        user.setPassword(passwordEncoder.encode("1"));
        userRepository.save(user);



    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ModelAndView logout() {
        authenticationService.logout();
        return new ModelAndView("redirect:" + "/login");
    }

}

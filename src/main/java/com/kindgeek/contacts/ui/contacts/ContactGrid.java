package com.kindgeek.contacts.ui.contacts;

import com.kindgeek.contacts.entity.Contact;
import com.kindgeek.contacts.service.ContactService;
import com.vaadin.data.Binder;
import com.vaadin.data.ValueProvider;
import com.vaadin.server.Setter;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.renderers.ButtonRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Yurii Ploskyi on 04.03.17.
 */
@Component
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class ContactGrid extends Grid<Contact> {

    @Autowired
    private ContactService contactService;

    private Contact contact = new Contact();
    private Binder<Contact> binder = getEditor().getBinder();

    private ContactsPage contactsPage;

    public ContactGrid() {
        bindFields();
        configureEditor();
        addDeleteButton();
        setSizeFull();
    }

    @PostConstruct
    public void updateGrid() {
        setItems(contactService.findAll());
    }

    public void updateGrid(Set<String> contactGroup) {
        setItems(contactService.findAll(contactGroup));
    }

    public void updateGrid(String like, String group) {
        List<Contact> contacts = contactService.findLike(like);
        if (group != null) {
            try {
                List<Contact> c = contacts.stream().filter(contact1 -> contact1.getContactGroup().equals(group)).collect(Collectors.toList());
                setItems(c);
            } catch (Exception e) {
                setItems(contacts);
            }
        } else {
            setItems(contacts);
        }
    }

    private void bindFields() {
        addColumns();
        binder.setBean(contact);
    }

    public ContactsPage getContactsPage() {
        return contactsPage;
    }

    public void setContactsPage(ContactsPage contactsPage) {
        this.contactsPage = contactsPage;
    }

    private void addColumns() {
        addColumn(Contact::getFirsName, Contact::setFirsName, Captions.FIRST_NAME);
        addColumn(Contact::getLastName, Contact::setLastName, Captions.LAST_NAME);
        addColumn(Contact::getEmail, Contact::setEmail, Captions.EMAIL);
        addColumn(Contact::getPhoneNumber, Contact::setPhoneNumber, Captions.PNONE_NUMBER);
        addColumn(Contact::getIdAsString, Contact::setIdAsString, Captions.ID);
        addColumn(Contact::getContactGroup, Contact::setContactGroup, Captions.GROUP);
    }

    private void addColumn(ValueProvider<Contact, String> getter, Setter<Contact, String> setter, String caption) {
        addColumn(getter, setter, new TextField(), caption);
    }

    private void addColumn(ValueProvider<Contact, String> getter, Setter<Contact, String> setter, TextField textField, String caption) {
        addColumn(getter).setEditorComponent(textField, setter).setExpandRatio(1).setCaption(caption);
        binder.forField(textField).bind(getter, setter);
    }

    private void configureEditor() {
        getEditor().setEnabled(true);
        getEditor().addSaveListener(editorSaveEvent -> edit());
        hideIdColumn();
    }

    private void addDeleteButton() {
        addColumn(selectedContact -> Captions.DELETE, new ButtonRenderer<>(clickEvent -> {
            if (getEditor() == null) {
                return;
            }
            delete(clickEvent.getItem());
            contactsPage.updateGroups();
            updateGrid();
        }));
    }

    private void hideIdColumn() {
        getColumns().stream().filter(contactColumn -> contactColumn.getCaption().equals(Captions.ID))
                .findFirst().get().setHidden(true);
    }

    private void delete(Contact contact) {
        contactService.delete(contact);
        updateGrid();
    }

    private void edit() {


        String firstNameValue = contact.getFirsName();
        String lastNameValue = contact.getLastName();
        String emailValue = contact.getEmail();
        String phoneNumberValue = contact.getPhoneNumber();

        if (isEmpty(firstNameValue) && isEmpty(lastNameValue) && isEmpty(emailValue) && isEmpty(phoneNumberValue)) {
            updateGrid();
            Notification.show("Enter at least one field about contact", Notification.Type.WARNING_MESSAGE);
            updateGrid();
            contactsPage.updateGroups();
            return;
        }

        if (firstNameValue != null) {
            if (firstNameValue.matches(".*\\d+.*")) {
                updateGrid();
                Notification.show("First name can not contains digits", Notification.Type.WARNING_MESSAGE);
                updateGrid();
                contactsPage.updateGroups();
                return;
            }
        }


        if (lastNameValue != null) {
            if (lastNameValue.matches(".*\\d+.*")) {
                updateGrid();
                Notification.show("Last name can not contains digits", Notification.Type.WARNING_MESSAGE);
                updateGrid();
                contactsPage.updateGroups();
                return;
            }
        }


        if (emailValue != null && !emailValue.isEmpty()) {
            if (!isValidEmail(emailValue)) {
                updateGrid();
                Notification.show("Email not valid", Notification.Type.WARNING_MESSAGE);
                updateGrid();
                contactsPage.updateGroups();
                return;
            }
        }


        if (phoneNumberValue != null && !phoneNumberValue.isEmpty()) {
            if (!phoneNumberValue.matches("\\d+")) {
                updateGrid();
                Notification.show("Phone number must contain digits only", Notification.Type.WARNING_MESSAGE);
                updateGrid();
                contactsPage.updateGroups();
                return;
            }
        }

        contactService.save(contact);
        updateGrid();
        contactsPage.updateGroups();
    }

    public void updateWithSet() {

    }

    private class Captions {
        public static final String FIRST_NAME = "First Name";
        public static final String LAST_NAME = "Last Name";
        public static final String EMAIL = "Email";
        public static final String PNONE_NUMBER = "Phone number";
        public static final String ID = "Id";
        public static final String GROUP = "Group";
        public static final String DELETE = "Delete";
    }

    private boolean isEmpty(String field) {
        return field == null || field.isEmpty();
    }

    public static boolean isValidEmail(String email) {
        if (email != null) {
            Pattern p = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
            Matcher m = p.matcher(email);
            return m.find();
        }
        return false;
    }
}

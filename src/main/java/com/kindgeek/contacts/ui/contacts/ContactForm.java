package com.kindgeek.contacts.ui.contacts;

import com.kindgeek.contacts.entity.Contact;
import com.kindgeek.contacts.service.ContactService;
import com.vaadin.data.Binder;
import com.vaadin.data.ValueProvider;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.Setter;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PopupView;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Yurii Ploskyi on 04.03.17.
 */
@Component
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class ContactForm extends FormLayout {

    @Autowired
    private ContactService contactService;

    @Autowired
    private ContactGrid contactGrid;
    private ContactsPage contactsPage;

    private List<String> allGroups;
    private String newGroupName = null;

    private Button save = new Button("Save", this::save);
    private Button cancel = new Button("Cancel", this::cancel);
    private TextField firstName = new TextField("First name");
    private TextField lastName = new TextField("Last name");
    private TextField phoneNumber = new TextField("Phone");
    private TextField email = new TextField("Email");
    private TextField groupForJava = new TextField("Group");
    private ComboBox group = new ComboBox("Group");
    private Binder<Contact> binder = new Binder<>(Contact.class);
    private Contact contact = new Contact();

    private PopupView popup;

    public void clean() {
        firstName.setValue("");
        lastName.setValue("");
        phoneNumber.setValue("");
        email.setValue("");
        allGroups = contactService.findAllGroups();
        group.setItems(allGroups);
        ComboBox.NewItemHandler newItemHandler = new ComboBox.NewItemHandler() {
            @Override
            public void accept(String s) {
                newGroupName = s;
                group.setValue(newGroupName);
            }
        };
        group.setNewItemHandler(newItemHandler);
        group.setTextInputAllowed(true);
        groupForJava.setValue("");
    }

    public void init() {
        configureComponents();
        buildLayout();
    }

    public PopupView getPopup() {
        return popup;
    }

    public void setPopup(PopupView popup) {
        this.popup = popup;
    }

    public ContactsPage getContactsPage() {
        return contactsPage;
    }

    public void setContactsPage(ContactsPage contactsPage) {
        this.contactsPage = contactsPage;
    }

    private void configureComponents() {
        save.setStyleName(ValoTheme.BUTTON_PRIMARY);
        save.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        setVisible(true);
    }

    private void buildLayout() {
        setSizeUndefined();
        setMargin(true);

        HorizontalLayout actions = new HorizontalLayout(save, cancel);
        save.setSizeFull();
        cancel.setSizeFull();
        actions.setSpacing(true);

        addComponents(firstName, lastName, email, phoneNumber, group, actions);
        firstName.setSizeFull();
        lastName.setSizeFull();
        email.setSizeFull();
        phoneNumber.setSizeFull();
        group.setSizeFull();
        actions.setSizeFull();

        bind();
    }

    private void bind() {
        binder.setBean(contact);
        bind(firstName, Contact::getFirsName, Contact::setFirsName);
        bind(lastName, Contact::getLastName, Contact::setLastName);
        bind(email, Contact::getEmail, Contact::setEmail);
        bind(phoneNumber, Contact::getPhoneNumber, Contact::setPhoneNumber);
        bind(groupForJava, Contact::getContactGroup, Contact::setContactGroup);
    }

    private void bind(TextField field, ValueProvider<Contact, String> getter, Setter<Contact, String> setter) {

        binder.forField(field).bind(getter, setter);
    }


    public void save(Button.ClickEvent event) {
        try {

            String firstNameValue = firstName.getValue();
            String lastNameValue = lastName.getValue();
            String emailValue = email.getValue();
            String phoneNumberValue = phoneNumber.getValue();

            if (group.getValue() != null &&
                    group.getValue().toString() != null &&
                    !group.getValue().toString().isEmpty()) {
                groupForJava.setValue(group.getValue().toString());
                if (!allGroups.contains(group.getValue().toString())) {
                    allGroups.add(group.getValue().toString());
                    group.setItems(allGroups);
                }
            }

            if (isEmpty(firstNameValue) && isEmpty(lastNameValue) && isEmpty(emailValue) && isEmpty(phoneNumberValue)) {
                Notification.show("Enter at least one field about contact", Notification.Type.WARNING_MESSAGE);
                return;
            }

            if (firstNameValue != null) {
                if (firstNameValue.matches(".*\\d+.*")) {
                    Notification.show("First name can not contains digits", Notification.Type.WARNING_MESSAGE);
                    return;
                }
            }


            if (lastNameValue != null) {
                if (lastNameValue.matches(".*\\d+.*")) {
                    Notification.show("Last name can not contains digits", Notification.Type.WARNING_MESSAGE);
                    return;
                }
            }


            if (emailValue != null && !emailValue.isEmpty()) {
                if (!isValidEmail(emailValue)) {
                    Notification.show("Email not valid", Notification.Type.WARNING_MESSAGE);
                    return;
                }
            }


            if (phoneNumberValue != null && !phoneNumberValue.isEmpty()) {
                if (!phoneNumberValue.matches("\\d+")) {
                    Notification.show("Phone number must contain digits only", Notification.Type.WARNING_MESSAGE);
                    return;
                }
            }


            contact.setId(null);

            contactService.save(contact);
            popup.setPopupVisible(false);
            contactGrid.updateGrid();
            contactsPage.updateGroups();
            Notification.show("Contact added", Notification.Type.WARNING_MESSAGE);
            contactGrid.updateGrid();
        } catch (Exception e) {
            System.out.println("--Catched   ");
            e.printStackTrace();
            Notification.show("Error while adding contact", Notification.Type.WARNING_MESSAGE);
        }

    }

    private boolean isEmpty(String field) {
        return field == null || field.isEmpty();
    }

    public static boolean isValidEmail(String email) {
        if (email != null) {
            Pattern p = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
            Matcher m = p.matcher(email);
            return m.find();
        }
        return false;
    }

    public void cancel(Button.ClickEvent event) {
        popup.setPopupVisible(false);
    }

}
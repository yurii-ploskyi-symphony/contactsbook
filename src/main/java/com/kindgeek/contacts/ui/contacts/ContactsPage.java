package com.kindgeek.contacts.ui.contacts;

import com.kindgeek.contacts.entity.Contact;
import com.kindgeek.contacts.service.ContactService;
import com.kindgeek.contacts.ui.util.PageNavigator;
import com.vaadin.annotations.Theme;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Resource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Yurii Ploskyi on 04.03.17.
 */
@SpringUI(path = "/contacts")
@Theme("valo")
public class ContactsPage extends UI {

    @Autowired
    private Grid<Contact> contactGrid;

    @Autowired
    private ContactService contactService;

    @Autowired
    private ContactForm contactForm;


    private PopupView editContactPopup;
    private VerticalLayout layout = new VerticalLayout();
    private ListSelect<String> listSelect;
    private Label group;
    private String selectedGroup;
    private TextField search;

    public String getSelectedGroup() {
        return selectedGroup;
    }

    public void setSelectedGroup(String selectedGroup) {
        this.selectedGroup = selectedGroup;
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        contactForm.init();
        contactForm.setContactsPage(this);
        contactGrid.setSizeFull();
        addEditView();
        layout.addComponent(getHeader());
        layout.addComponent(editContactPopup);
        layout.setComponentAlignment(editContactPopup, Alignment.MIDDLE_CENTER);
        editContactPopup.setSizeFull();
        editContactPopup.setHideOnMouseOut(false);
        HorizontalLayout content = getContactsContent();
        layout.addComponent(content);
        layout.setComponentAlignment(content, Alignment.TOP_CENTER);
        ((ContactGrid) contactGrid).setContactsPage(this);
        contactGrid.setHeight("500px");
        setContent(layout);
    }

    private void addEditView() {
        editContactPopup = new PopupView(null, contactForm);
        editContactPopup.setSizeFull();
        contactForm.setPopup(editContactPopup);
    }

    private Image getIconImage() {
        Resource iconResource = new ExternalResource("http://icons.iconarchive.com/icons/graphicloads/100-flat/256/contact-icon.png");
        Image image = new Image();
        image.setSource(iconResource);
        return image;
    }

    private HorizontalLayout getHeader() {
        HorizontalLayout header = new HorizontalLayout();

        Image icon = getIconImage();

        header.addComponent(icon);
        header.setComponentAlignment(icon, Alignment.MIDDLE_CENTER);

        header.setExpandRatio(icon, 1);
        header.setMargin(false);
        VerticalLayout headerRows = getHeaderRows();
        header.addComponent(headerRows);
        headerRows.setMargin(false);
        header.setExpandRatio(headerRows, 4);
        header.setSizeFull();

        header.setSpacing(true);
        return header;
    }

    private VerticalLayout getHeaderRows() {
        VerticalLayout headerRows = new VerticalLayout();
        headerRows.addComponent(getFirstRow());
        group = new Label();
        Button logout = new Button("logout", clickEvent -> PageNavigator.navigateToLogout(getUI()));
        logout.setStyleName(ValoTheme.BUTTON_LINK);
        headerRows.addComponent(logout);
        headerRows.setComponentAlignment(logout, Alignment.MIDDLE_RIGHT);
        headerRows.addComponent(group);
        headerRows.addComponent(new Label());
        headerRows.setComponentAlignment(group, Alignment.MIDDLE_LEFT);
        headerRows.setSizeFull();
        headerRows.setHeight("100%");
        headerRows.setWidth("100%");
        HorizontalLayout thirdRow = getThirdRow();
        headerRows.addComponent(thirdRow);
        headerRows.setComponentAlignment(thirdRow, Alignment.BOTTOM_CENTER);
        return headerRows;
    }

    private HorizontalLayout getFirstRow() {
        HorizontalLayout row = new HorizontalLayout();
        Label title = new Label("Contact list");
        title.setStyleName(ValoTheme.LABEL_HUGE);
        row.addComponent(title);
        row.setComponentAlignment(title, Alignment.MIDDLE_LEFT);

        Label username = new Label(getCurrentUserName());
        row.addComponent(username);
        row.setComponentAlignment(username, Alignment.MIDDLE_RIGHT);

        row.setSizeFull();
        row.setWidth("100%");

        return row;
    }

    private HorizontalLayout getThirdRow() {
        HorizontalLayout row = new HorizontalLayout();
        Image icon = new Image();
        icon.setSource(new ExternalResource("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAABzElEQVRIS7WVgTEEQRBF30VABogAESACLgMyIAMZIAJE4ETARUAGyIAIqKemr+bmZnfn1Ooq5e52Zl7/7t+zE7pjGzgGDoHNtOwTeAYegfeevSuPJpXFAq6Ak4GDZsBFK7AEefhtUvABXCcFrwm6lxSeA1uACs8Aob2Rg4Q8pNVmKqQvhKncmA7BAmS5XpKSfSAUDCWqQvepzH2dfQuQ0m18i5ISHsrugdOuzASp5g2wJ37+S6jEnu10qRIUGYUaYWtZt3JG1d5RtujNJeDfOhG9cr6qY6EiB/AAiH75XZWthoiEvoF5sn9VUQ3kb+uqGgSVpROymxqrbVuiqXSlGbSot4PAoxZKqxnC3jpNexr2R1X+d+qHXNhkbw8uB9bbOkro87u0xlI6bzk4KtLpOA/IryCz30hXSTjOMmpXwUJMSGhE9OYL8PPgFeTG/FI1y5uB/uSXqklY4nBe1d75j8LMWGVm5w3ubOSvCWdOiL1VieH6CNdqoiXHdr34BHjJ9oWXqLMm8KlYuAKrgWKPB6iw9iq3V9EPn5egcO5CWR+ocYSWxqHcs1A2Fqgchxz4CxsL5MF9sMcxQX2w+digLtj0P0ABcx4tp6My+wHMeHzrbngtmgAAAABJRU5ErkJggg=="));
        HorizontalLayout v = new HorizontalLayout();


        search = new TextField();
        search.setWidth("300px");
        search.addValueChangeListener(valueChangeEvent -> {
            ((ContactGrid) contactGrid).updateGrid(valueChangeEvent.getValue(), selectedGroup);
        });
//        row.addComponent(search);
//        row.setComponentAlignment(search, Alignment.MIDDLE_LEFT);
        v.addComponents(icon, search);
        v.setComponentAlignment(search, Alignment.MIDDLE_LEFT);
        v.setComponentAlignment(icon, Alignment.MIDDLE_CENTER);

        row.addComponent(v);

        row.setComponentAlignment(v, Alignment.MIDDLE_LEFT);

        editContactPopup = new PopupView(null, contactForm);
        editContactPopup.setSizeFull();
        contactForm.setPopup(editContactPopup);

        Button addNew = new Button("Create contact", click -> {
            editContactPopup.setPopupVisible(true);

            Component content = editContactPopup.getContent().getPopupComponent();

            ((ContactForm) content).clean();


        });
        addNew.setWidth("300px");
        addNew.setStyleName(ValoTheme.BUTTON_PRIMARY);

        row.addComponents(addNew);
        row.setComponentAlignment(addNew, Alignment.MIDDLE_RIGHT);

        row.setSizeFull();
        row.setWidth("100%");


        return row;
    }

    private HorizontalLayout getContactsContent() {
        HorizontalLayout content = new HorizontalLayout();

        VerticalLayout verticalLayout = new VerticalLayout();
        listSelect = new ListSelect<>();

        updateGroups();
//        listSelect.setWidth(309, Unit.PIXELS);
//        listSelect.setHeight("500/px");
        listSelect.setSizeFull();
        Button button = new Button("All groups", clickEvent -> {

            ((ContactGrid) contactGrid).updateGrid();
            selectedGroup = null;
            group.setValue("");


        });
        button.setWidth("100%");

        verticalLayout.addComponents(button, listSelect);

        verticalLayout.setExpandRatio(button, 1);
        verticalLayout.setExpandRatio(listSelect, 10);
        verticalLayout.setSpacing(true);
        verticalLayout.setSizeFull();
        verticalLayout.setMargin(false);
        content.addComponent(verticalLayout);
        content.setExpandRatio(verticalLayout, 1);

        listSelect.addValueChangeListener(valueChangeEvent -> {
//            if (valueChangeEvent.getValue().size() > 1) {
//                return;
//            }
            String oldValue = selectedGroup;
            try {
                selectedGroup = valueChangeEvent.getValue().iterator().next();
                search.setValue("");
            } catch (Exception e) {
                selectedGroup = null;
                e.printStackTrace();
                Set<String> s = new HashSet<>();
                s.add(oldValue);
                ((ContactGrid) contactGrid).updateGrid(s);
                return;
            }
            ((ContactGrid) contactGrid).updateGrid(valueChangeEvent.getValue());
            try {
                group.setValue(valueChangeEvent.getValue().iterator().next());
            } catch (Exception e) {

                e.printStackTrace();
            }
        });
//        listSelect.setWidth("100px");
        content.addComponent(contactGrid);
        content.setExpandRatio(contactGrid, 4);
        contactGrid.setSizeFull();
        content.setWidth(100, Unit.PERCENTAGE);
        return content;
    }

    private String getCurrentUserName() {
        try {
            return ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        } catch (Exception e) {
            return null;
        }
    }

    public void updateGroups() {
        List<String> allGroups = contactService.findAllGroups();
        if (allGroups != null) {
            listSelect.setItems(allGroups);
        }
    }

}
package com.kindgeek.contacts.ui.register;

import com.kindgeek.contacts.ui.common.PageWithIcon;
import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Alignment;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Yurii Ploskyi on 05.03.17.
 */
@SpringUI(path = "/register")
@Theme("valo")
public class RegisterPage extends PageWithIcon {

    @Autowired
    private RegisterForm registerForm;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        registerForm.init();
        layout.addComponent(registerForm);
        setContent(layout);
        layout.setComponentAlignment(registerForm, Alignment.TOP_CENTER);
    }

}

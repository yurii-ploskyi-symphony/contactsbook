package com.kindgeek.contacts.ui.register;

import com.kindgeek.contacts.entity.User;
import com.kindgeek.contacts.service.AuthenticationService;
import com.kindgeek.contacts.service.UserService;
import com.kindgeek.contacts.service.impl.UsernameExsistException;
import com.kindgeek.contacts.ui.util.PageNavigator;
import com.vaadin.data.Binder;
import com.vaadin.data.ValueProvider;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.Setter;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * Created by Yurii Ploskyi on 04.03.17.
 */
@Component
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class RegisterForm extends FormLayout {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private UserService userService;

    private User user = new User();
    private Binder<User> binder = new Binder<>(User.class);
    private Button register = new Button("Register", this::register);
    private Button login = new Button("login", this::login);

    private TextField username = new TextField("Username");
    private TextField password = new PasswordField("Password");
    private TextField repeatPassword = new PasswordField("Password");

    public void init() {
        configureComponents();
        buildLayout();
        bind();
    }

    private void bind() {
        binder.setBean(user);
        bind(username, User::getUserName, User::setUserName);
        bind(password, User::getPassword, User::setPassword);
    }

    private void bind(TextField field, ValueProvider<User, String> getter, Setter<User, String> setter) {
        binder.forField(field).bind(getter, setter);
    }

    private void configureComponents() {
        register.setStyleName(ValoTheme.BUTTON_PRIMARY);
        register.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        setVisible(true);
        setSizeFull();
    }

    private void buildLayout() {
        setSizeUndefined();
        setMargin(true);
        HorizontalLayout actions = new HorizontalLayout(register, login);
        actions.setSpacing(true);
        addComponents(username, password, repeatPassword, actions);
    }


    private void register(Button.ClickEvent event) {
        try {
            if (password.getValue().equals(repeatPassword.getValue())) {




                userService.register(user);
                authenticationService.authenticate(user);
                PageNavigator.navigateToContacts(getUI());
            } else {
                Notification.show("Passwords don't match", Notification.Type.WARNING_MESSAGE);
            }
        } catch (UsernameExsistException e) {
            Notification.show("Such username already exist", Notification.Type.WARNING_MESSAGE);
        } catch (Exception e) {
            Notification.show("Enter all values", Notification.Type.WARNING_MESSAGE);
        }
    }

    private void login(Button.ClickEvent event) {
        PageNavigator.navigateToLogin(getUI());
    }

//    private void validatePasswords() {
//        try {
//            if (password.getValue() != repeatPassword.getValue()) {
//                Notification.show("Passwords don't match", Notification.Type.WARNING_MESSAGE);
//            }
//        } catch (NullPointerException e) {
//            Notification.show("Passwords don't match", Notification.Type.WARNING_MESSAGE);
//        }
//    }

}
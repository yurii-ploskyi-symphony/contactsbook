package com.kindgeek.contacts.ui.login;

import com.kindgeek.contacts.ui.common.PageWithIcon;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Alignment;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Yurii Ploskyi on 05.03.17.
 */
@SpringUI(path = "/login")
public class LoginPage extends PageWithIcon {

    @Autowired
    private LoginForm loginForm;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        loginForm.init();
        layout.addComponent(loginForm);
        setContent(layout);
        layout.setComponentAlignment(loginForm, Alignment.TOP_CENTER);
    }

}

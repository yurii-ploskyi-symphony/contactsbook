package com.kindgeek.contacts.ui.login;

import com.kindgeek.contacts.entity.User;
import com.kindgeek.contacts.service.AuthenticationService;
import com.kindgeek.contacts.ui.util.PageNavigator;
import com.vaadin.data.Binder;
import com.vaadin.data.ValueProvider;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.Setter;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * Created by Yurii Ploskyi on 04.03.17.
 */
@Component
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class LoginForm extends FormLayout {

    @Autowired
    private AuthenticationService authenticationService;

    private User user = new User();
    private Binder<User> binder = new Binder<>(User.class);
    private Button login = new Button("Login", this::login);
    private Button signup = new Button("Register", this::register);

    private TextField username = new TextField("Username");
    private TextField password = new PasswordField("Password");

    public void init() {
        configureComponents();
        buildLayout();
        bind();
    }

    private void bind() {
        binder.setBean(user);
        bind(username, User::getUserName, User::setUserName);
        bind(password, User::getPassword, User::setPassword);
    }

    private void bind(TextField field, ValueProvider<User, String> getter, Setter<User, String> setter) {
        binder.forField(field).bind(getter, setter);
    }

    private void configureComponents() {
        login.setStyleName(ValoTheme.BUTTON_PRIMARY);
        login.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        setVisible(true);
        setSizeFull();
    }

    private void buildLayout() {
        setSizeUndefined();
        setMargin(true);
        HorizontalLayout actions = new HorizontalLayout(login, signup);
        actions.setSpacing(true);
        addComponents(username, password, actions);
    }


    public void login(Button.ClickEvent event) {
        try {
            authenticationService.authenticate(user);
            PageNavigator.navigateToContacts(getUI());
        } catch (Exception e) {
            Notification.show("Bad credentials", Notification.Type.WARNING_MESSAGE);
        }
    }

    public void register(Button.ClickEvent event) {
        PageNavigator.navigateToRegister(getUI());
    }

}
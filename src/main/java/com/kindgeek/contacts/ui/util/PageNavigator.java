package com.kindgeek.contacts.ui.util;

import com.vaadin.server.Page;
import com.vaadin.ui.UI;

/**
 * Created by Yurii Ploskyi on 05.03.17.
 */
public class PageNavigator {

    private static final String CONTACTS_PATH = "/contacts";
    private static final String REGISTER_PATH = "/register";
    private static final String LOGIN_PATH = "/login";
    private static final String LOGOUT_PATH = "/logout";
    private static final String PREFIX = "://";

    public static void navigateToContacts(UI ui) {
        ui.getPage().setLocation(getContactsUrl());
    }

    public static void navigateToRegister(UI ui) {
        ui.getPage().setLocation(getRegisterUrl());
    }

    public static void navigateToLogin(UI ui) {
        ui.getPage().setLocation(getLoginUrl());
    }

    public static void navigateToLogout(UI ui) {
        ui.getPage().setLocation(getLogoutUrl());
    }

    private static String getContactsUrl() {
        return getBaseUrl() + CONTACTS_PATH;
    }

    private static String getRegisterUrl() {
        return getBaseUrl() + REGISTER_PATH;
    }

    private static String getLoginUrl() {
        return getBaseUrl() + LOGIN_PATH;
    }

    private static String getLogoutUrl() {
        return getBaseUrl() + LOGOUT_PATH;
    }

    private static String getBaseUrl() {
        return Page.getCurrent().getLocation().getScheme() + PREFIX + Page.getCurrent().getLocation().getAuthority();
    }

}

package com.kindgeek.contacts.ui.common;

import com.vaadin.annotations.Theme;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Resource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Image;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.springframework.stereotype.Component;

/**
 * Created by Yurii Ploskyi on 05.03.17.
 */
@Component
@Theme("valo")
public abstract class PageWithIcon extends UI {

    protected VerticalLayout layout = new VerticalLayout();
    private static final String iconUrl = "http://icons.iconarchive.com/icons/graphicloads/100-flat/256/contact-icon.png";
    private Resource iconResource = new ExternalResource(iconUrl);
    protected Image iconImage = new Image();

    public PageWithIcon() {
        iconImage.setSource(iconResource);
        iconImage.setSizeUndefined();
        layout.addComponent(iconImage);
        layout.setComponentAlignment(iconImage, Alignment.MIDDLE_CENTER);
    }

}
